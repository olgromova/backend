package ru.itmo.backend.person.service.impl;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.person.exceptions.AlreadyExistsException;
import ru.itmo.backend.person.exceptions.BadRequestException;
import ru.itmo.backend.person.exceptions.DBException;
import ru.itmo.backend.person.repository.PersonRepository;
import ru.itmo.backend.person.service.AddressService;
import ru.itmo.backend.person.service.ContactService;
import ru.itmo.backend.person.service.IdentityDocumentService;
import ru.itmo.backend.person.dto.AddressCreateRequest;
import ru.itmo.backend.person.dto.AddressDto;
import ru.itmo.backend.person.dto.AddressUpdatePersonRequest;
import ru.itmo.backend.person.dto.ContactDto;
import ru.itmo.backend.person.dto.IdentityDocumentDto;
import ru.itmo.backend.person.dto.PersonCreateRequest;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.dto.PersonDtoSimple;
import ru.itmo.backend.person.dto.PersonUpdateRequest;
import ru.itmo.backend.person.mapper.AddressMapper;
import ru.itmo.backend.person.mapper.ContactMapper;
import ru.itmo.backend.person.mapper.IdentityDocumentMapper;
import ru.itmo.backend.person.mapper.PersonMapper;
import ru.itmo.backend.person.mapper.RegionMapper;
import ru.itmo.backend.person.model.Address;
import ru.itmo.backend.person.model.Contact;
import ru.itmo.backend.person.model.IdentityDocument;
import ru.itmo.backend.person.model.Person;
import ru.itmo.backend.person.service.PersonService;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    private static final PageRequest STANDARD_PAGE_REQUEST = PageRequest.of(0, 50);

    private final PersonRepository personRepository;

    private final IdentityDocumentService identityDocumentService;
    private final ContactService contactService;
    private final AddressService addressService;

    private final PersonMapper personMapper;
    private final AddressMapper addressMapper;
    private final RegionMapper regionMapper;
    private final IdentityDocumentMapper identityDocumentMapper;
    private final ContactMapper contactMapper;

    public PersonServiceImpl(
        PersonRepository personRepository,
        IdentityDocumentService identityDocumentService,
        PersonMapper personMapper,
        ContactService contactService,
        AddressService addressService,
        AddressMapper addressMapper,
        RegionMapper regionMapper,
        IdentityDocumentMapper identityDocumentMapper,
        ContactMapper contactMapper
    ) {
        this.personRepository = personRepository;
        this.identityDocumentService = identityDocumentService;
        this.personMapper = personMapper;
        this.contactService = contactService;
        this.addressService = addressService;
        this.addressMapper = addressMapper;
        this.regionMapper = regionMapper;
        this.identityDocumentMapper = identityDocumentMapper;
        this.contactMapper = contactMapper;
    }

    @Override
    @Transactional
    public PersonDto create(PersonCreateRequest personCreateRequest) {
        try {
            AddressCreateRequest mainAddresses = personCreateRequest.getAddresses()
                .stream()
                .filter(AddressCreateRequest::isMain)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("something went wrong"));
            Person person = personRepository.save(personMapper.toPerson(personCreateRequest));
            PersonDto personDto = personMapper.toPersonResponse(person);
            List<IdentityDocumentDto> documents = identityDocumentService.create(
                personCreateRequest.getDocuments(),
                personMapper.toPersonResponse(person)
            );
            List<ContactDto> contacts = contactService.create(
                personCreateRequest.getContacts(),
                personDto
            );
            List<AddressDto> addresses = addressService.create(
                personCreateRequest.getAddresses(),
                personDto
            );
            AddressDto mainAddressDto = addresses.stream()
                .filter(it -> Objects.equals(mainAddresses.getName(), it.getName())
                    && Objects.equals(mainAddresses.getRegion(), it.getRegion().getName()))
                .findFirst()
                .orElseThrow(() -> new BadRequestException("Only one registered address should be"));
            Address mainAddress = addressMapper.fromDto(mainAddressDto);
            personRepository.updateMainAddress(person.getPersonId(), mainAddress);
            person.setMainAddress(mainAddress);
            return personMapper.toPersonResponse(person, documents, contacts, addresses);
        } catch (DBException | BadRequestException | AlreadyExistsException e) {
            throw e;
        } catch (Exception e) {
            throw new DBException("Can't create person: " + personCreateRequest.getName());
        }
    }

    @Override
    @Transactional
    public PersonDto find(String id) {
        try {
            Person person = personRepository.findById(id)
                .orElseThrow(() -> new BadRequestException("Person with id: " + id + " not found"));
            return toPersonDto(person);
        } catch (BadRequestException | DBException e) {
            throw e;
        } catch (Exception e) {
            throw new DBException("Something went wrong. Can't find person: " + id);
        }
    }

    @Override
    public boolean verify(String name, String passport) {
        try {
            return personRepository.existsMainDocument(name, passport);
        } catch (Exception e) {
            throw new DBException("Can't verify person: " + name);
        }
    }

    @Override
    @Transactional
    public PersonDto update(PersonUpdateRequest personUpdateRequest) {
        try {
            AddressUpdatePersonRequest mainAddresses = personUpdateRequest.getAddresses()
                .stream()
                .filter(AddressUpdatePersonRequest::isMain)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("something went wrong."));
            Person person = personMapper.toPerson(personUpdateRequest);
            List<AddressDto> addresses = addressService.updatePerson(
                personUpdateRequest.getAddresses(),
                personMapper.toPersonResponse(person)
            );
            person.setAddresses(addresses.stream().map(addressMapper::fromDto).collect(Collectors.toSet()));
            Address mainAddress = findMainAddress(
                addresses,
                mainAddresses.getName(),
                mainAddresses.getRegion()
            );
            person.setMainAddress(mainAddress);
            return toPersonDto(personRepository.save(person));
        } catch (DBException | BadRequestException e) {
            throw e;
        } catch (Exception e) {
            throw new DBException("Can't update person " + personUpdateRequest.getPersonId());
        }
    }

    @Override
    public List<PersonDtoSimple> findAll(Integer page, Integer size, String region) {
        try {
            Set<String> ids;
            if (page == null || size == null) {
                if (region == null) {
                    ids = new HashSet<>(personRepository.findAllIdsByShowTrue(STANDARD_PAGE_REQUEST));
                } else {
                    ids = new HashSet<>(personRepository.findAllIdsByRegionAndShowTrue(region, STANDARD_PAGE_REQUEST));
                }
            } else {
                if (region == null) {
                    ids = new HashSet<>(personRepository.findAllIdsByShowTrue(PageRequest.of(page, size)));
                } else {
                    ids = new HashSet<>(personRepository.findAllIdsByRegionAndShowTrue(
                        region,
                        PageRequest.of(page, size)
                    ));
                }
            }
            List<Person> result = personRepository.findAllPersonsInIds(ids);
            return result.stream()
                .map(this::toPersonSimple)
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't find persons");
        }
    }

    @Override
    public PersonDto findByPassport(String passport) {
        try {
            var person = personRepository.findByPassport(passport);
            if (person.isEmpty()) {
                throw new BadRequestException("person with passport " + passport + " not found");
            }
            return toPersonDto(person.get());
        } catch (Exception e) {
            throw new DBException("Can't find person by passport");
        }
    }

    private PersonDto toPersonDto(Person person) {
        List<IdentityDocumentDto> documents = person.getDocuments().stream()
            .map(it -> identityDocumentMapper.toIdentityDocumentResponse(
                it,
                person.getPersonId()
            ))
            .collect(Collectors.toList());
        List<ContactDto> contacts = person.getContacts().stream()
            .map(contactMapper::toContactResponse)
            .collect(Collectors.toList());
        List<AddressDto> addresses = person.getAddresses().stream()
            .map(it -> addressMapper.toAddressResponse(it, regionMapper.toRegionResponse(it.getRegion())))
            .collect(Collectors.toList());
        return personMapper.toPersonResponse(
            person,
            documents,
            contacts,
            addresses
        );
    }

    private Address findMainAddress(List<AddressDto> addresses, String name, String region) {
        AddressDto mainAddressDto = addresses.stream()
            .filter(it -> Objects.equals(name, it.getName())
                && Objects.equals(region, it.getRegion().getName()))
            .findFirst()
            .orElseThrow(() -> new BadRequestException("Only one registered address should be"));
        return addressMapper.fromDto(mainAddressDto);
    }

    private PersonDtoSimple toPersonSimple(Person person) {
        var passport = person.getDocuments()
            .stream()
            .filter(IdentityDocument::isMain)
            .findAny()
            .orElseThrow(() -> new RuntimeException("Can't find main document to person: " + person.getPersonId()));
        var phone = person.getContacts()
            .stream()
            .filter(it -> Contact.ContactType.PHONE.equals(it.getType()))
            .findAny()
            .orElseThrow(() -> new RuntimeException("Can't find phone to person: " + person.getPersonId()));
        return personMapper.toPersonSimpleResponse(
            personMapper.toPersonResponse(person),
            identityDocumentMapper.toIdentityDocumentResponse(passport, person.getPersonId()),
            contactMapper.toContactResponse(phone),
            addressMapper.toAddressResponse(
                person.getMainAddress(),
                regionMapper.toRegionResponse(person.getMainAddress().getRegion())
            )
        );
    }
}