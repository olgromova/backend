package ru.itmo.backend.person.service;

import ru.itmo.backend.person.dto.ContactCreateRequest;
import ru.itmo.backend.person.dto.ContactDto;
import ru.itmo.backend.person.dto.PersonDto;

import java.util.List;

public interface ContactService {
    List<ContactDto> findAll(String personId);

    List<ContactDto> create(List<ContactCreateRequest> contacts, PersonDto personDto);

    ContactDto findPhone(String personId);
}
