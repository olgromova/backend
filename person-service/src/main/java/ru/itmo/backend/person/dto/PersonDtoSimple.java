package ru.itmo.backend.person.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.Contact;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
public class PersonDtoSimple {
    private String personId;

    private String name;

    private LocalDate birthday;

    private Contact phone;

    private IdentityDocumentDto mainDocument;

    private AddressDto registrationAddress;
}
