package ru.itmo.backend.person.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.person.model.Address;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, String> {
    @Query("from address a join a.persons p where p.personId = :personId")
    List<Address> findAllByPersonId(@Param("personId") String personId);

    Optional<Address> findByName(String name);
}
