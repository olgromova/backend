package ru.itmo.backend.person.validators;


import ru.itmo.backend.person.validators.impl.OnlyOneMainValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = OnlyOneMainValidator.class)
public @interface OnlyOneMain {
    String message();

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
