package ru.itmo.backend.person.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.person.model.IdentityDocument;
import ru.itmo.backend.person.dto.IdentityDocumentCreateRequest;
import ru.itmo.backend.person.dto.IdentityDocumentDto;
import ru.itmo.backend.person.model.Person;

@Mapper(componentModel = "spring")
public interface IdentityDocumentMapper {
    @Mapping(target = "documentId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "person", source = "person")
    IdentityDocument toIdentityDocument(IdentityDocumentCreateRequest identityDocumentCreateRequest, Person person);

    @Mapping(target = "personId", source = "personId")
    IdentityDocumentDto toIdentityDocumentResponse(IdentityDocument identityDocument, String personId);
}
