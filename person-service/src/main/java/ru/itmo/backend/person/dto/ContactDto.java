package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.Contact;

@Getter
@Setter
public class ContactDto {
    private String contactId;

    private Contact.ContactType type;

    private String value;

    private String personId;
}
