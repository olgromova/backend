package ru.itmo.backend.person.dto;

public abstract class WithMain {
    public abstract boolean isMain();
}
