package ru.itmo.backend.person.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.person.model.IdentityDocument;

import java.util.List;

@Repository
public interface IdentityDocumentRepository extends JpaRepository<IdentityDocument, String> {
    @Query("from identity_document s where s.person.personId=:personId and s.main=true")
    IdentityDocument findMainDocument(@Param("personId") String personId);

    List<IdentityDocument> findAllByPerson_PersonId(String personId);
}
