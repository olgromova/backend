package ru.itmo.backend.person.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itmo.backend.person.exceptions.AlreadyExistsException;
import ru.itmo.backend.person.exceptions.BadRequestException;
import ru.itmo.backend.person.exceptions.DBException;

import java.time.LocalDateTime;
import java.util.Map;

@ControllerAdvice
public class ExceptionsControllerAdvice {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleException(BadRequestException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<?> handleException(AlreadyExistsException e) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(Map.of("error", e.getMessage()));
    }

    @ExceptionHandler(DBException.class)
    public ResponseEntity<?> handleException(DBException e) {
        return ResponseEntity.internalServerError().body(Map.of("error", e.getMessage()));
    }
}
