package ru.itmo.backend.person.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity(name = "contact")
@Table(name = "contacts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
    @Id
    private String contactId;

    @Column
    @Enumerated(EnumType.STRING)
    private ContactType type;

    @Column
    private String value;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    public enum ContactType {
        PHONE, EMAIL, WORK_PHONE
    }
}
