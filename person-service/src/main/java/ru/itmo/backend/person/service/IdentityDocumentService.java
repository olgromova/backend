package ru.itmo.backend.person.service;

import ru.itmo.backend.person.dto.IdentityDocumentCreateRequest;
import ru.itmo.backend.person.dto.IdentityDocumentDto;
import ru.itmo.backend.person.dto.PersonDto;

import java.util.List;

public interface IdentityDocumentService {
    List<IdentityDocumentDto> create(List<IdentityDocumentCreateRequest> identityDocumentCreateRequest, PersonDto person);

    IdentityDocumentDto mainDocument(String personId);

    List<IdentityDocumentDto> findAll(String personId);
}
