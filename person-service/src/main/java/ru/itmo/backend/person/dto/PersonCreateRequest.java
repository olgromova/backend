package ru.itmo.backend.person.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.validators.OnlyOneMain;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class PersonCreateRequest {
    @NotBlank
    private String name;

    @NotEmpty
    @OnlyOneMain(message = "only one main document must be")
    @Valid
    private List<IdentityDocumentCreateRequest> documents;

    @NotEmpty
    @OnlyOneMain(message = "only one main address must be")
    @Valid
    private List<AddressCreateRequest> addresses;

    @NotEmpty
    @Valid
    private List<ContactCreateRequest> contacts;

    @NotNull
    private LocalDate birthday;
}
