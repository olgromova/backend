package ru.itmo.backend.person.service;

import ru.itmo.backend.person.dto.AddressCreateRequest;
import ru.itmo.backend.person.dto.AddressDto;
import ru.itmo.backend.person.dto.AddressUpdatePersonRequest;
import ru.itmo.backend.person.dto.PersonDto;

import java.util.List;

public interface AddressService {
    List<AddressDto> findAll(String personId);

    List<AddressDto> create(List<AddressCreateRequest> addressCreateRequests, PersonDto personDto);

    List<AddressDto> updatePerson(List<AddressUpdatePersonRequest> addressUpdatePersonRequests, PersonDto personDto);
}
