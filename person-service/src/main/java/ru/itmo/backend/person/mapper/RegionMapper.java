package ru.itmo.backend.person.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.person.dto.RegionDto;
import ru.itmo.backend.person.model.Region;

@Mapper(componentModel = "spring")
public interface RegionMapper {
    RegionDto toRegionResponse(Region region);

    @Mapping(target = "regionId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "name", source = "name")
    Region toRegion(String name);
}
