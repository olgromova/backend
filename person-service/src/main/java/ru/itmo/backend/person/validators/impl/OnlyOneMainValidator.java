package ru.itmo.backend.person.validators.impl;

import ru.itmo.backend.person.validators.OnlyOneMain;
import ru.itmo.backend.person.dto.WithMain;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.List;

public class OnlyOneMainValidator implements
    ConstraintValidator<OnlyOneMain, List<? extends WithMain>> {
    @Override
    public void initialize(OnlyOneMain constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(List<? extends WithMain> value, ConstraintValidatorContext context) {
        return value.stream()
            .filter(WithMain::isMain)
            .count() == 1;
    }
}
