package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.IdentityDocument;

@Getter
@Setter
public class IdentityDocumentDto {
    private String documentId;

    private String number;

    private String personId;

    private boolean main;

    private IdentityDocument.DocumentType type;
}