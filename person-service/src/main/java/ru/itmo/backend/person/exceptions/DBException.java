package ru.itmo.backend.person.exceptions;

public class DBException extends RuntimeException {

    public DBException(String message) {
        super(message);
    }
}
