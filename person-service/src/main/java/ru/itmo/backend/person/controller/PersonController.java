package ru.itmo.backend.person.controller;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.backend.person.dto.PersonCreateRequest;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.dto.PersonDtoSimple;
import ru.itmo.backend.person.dto.PersonUpdateRequest;
import ru.itmo.backend.person.service.PersonService;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
@Validated
public class PersonController {
    private final PersonService personService;


    public PersonController(PersonService personService) {this.personService = personService;}

    @GetMapping
    public List<PersonDtoSimple> findAll(
        @RequestParam(value = "page", required = false) Integer page,
        @RequestParam(value = "size", required = false) Integer size,
        @RequestParam(value = "region", required = false) String region
    ) {
        return personService.findAll(page, size, region);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PersonDto create(@RequestBody @Valid PersonCreateRequest personCreateRequest) {
        return personService.create(personCreateRequest);
    }

    @GetMapping("/{id}")
    public PersonDto find(@PathVariable String id) {
        return personService.find(id);
    }

    @GetMapping("/verify")
    public boolean verify(@RequestParam("name") String name, @RequestParam("passport") String passport) {
        return personService.verify(name, passport);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public PersonDto update(@RequestBody @Valid PersonUpdateRequest request) {
        return personService.update(request);
    }

    @GetMapping("/info")
    public PersonDto findByPassport(@RequestParam("passport") String passport) {
        return personService.findByPassport(passport);
    }
}
