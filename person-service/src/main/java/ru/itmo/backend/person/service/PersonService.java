package ru.itmo.backend.person.service;


import ru.itmo.backend.person.dto.PersonCreateRequest;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.dto.PersonDtoSimple;
import ru.itmo.backend.person.dto.PersonUpdateRequest;

import java.util.List;

public interface PersonService {
    PersonDto create(PersonCreateRequest personCreateRequest);


    PersonDto find(String id);

    boolean verify(String name, String passport);

    PersonDto update(PersonUpdateRequest personUpdateRequest);

    List<PersonDtoSimple> findAll(Integer page, Integer size, String region);

    PersonDto findByPassport(String passport);
}
