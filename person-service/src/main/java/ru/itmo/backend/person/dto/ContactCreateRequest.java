package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.Contact;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Getter
@Setter
public class ContactCreateRequest {
    @NotNull
    private Contact.ContactType type;

    @NotBlank
    @Size(min = 1, max = 20)
    private String value;
}
