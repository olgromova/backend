package ru.itmo.backend.person.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@Entity(name = "person")
@Table(name = "persons")
@AllArgsConstructor
@NoArgsConstructor
//@NamedEntityGraph(name = "person-entity-graph",
//    attributeNodes = {
//        @NamedAttributeNode("addresses"),
//        @NamedAttributeNode("documents"),
//        @NamedAttributeNode("contacts"),
//        @NamedAttributeNode("mainAddress")
//    }
//)
public class Person {
    @Id
    private String personId;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_address_id")
    private Address mainAddress;

    @Column
    private LocalDate birthday;

    @Column
    private boolean show = true;

    @ManyToMany(mappedBy = "persons")
    private Set<Address> addresses;

    @OneToMany(mappedBy = "person")
    private Set<IdentityDocument> documents;

    @OneToMany(mappedBy = "person")
    private Set<Contact> contacts;
}
