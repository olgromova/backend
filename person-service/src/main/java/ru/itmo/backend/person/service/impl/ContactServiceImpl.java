package ru.itmo.backend.person.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.person.exceptions.AlreadyExistsException;
import ru.itmo.backend.person.exceptions.DBException;
import ru.itmo.backend.person.repository.ContactRepository;
import ru.itmo.backend.person.service.ContactService;
import ru.itmo.backend.person.dto.ContactCreateRequest;
import ru.itmo.backend.person.dto.ContactDto;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.mapper.ContactMapper;
import ru.itmo.backend.person.mapper.PersonMapper;
import ru.itmo.backend.person.model.Contact;
import ru.itmo.backend.person.model.Person;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl implements ContactService {
    private final ContactRepository contactRepository;
    private final ContactMapper contactMapper;
    private final PersonMapper personMapper;

    public ContactServiceImpl(
        ContactRepository contactRepository,
        ContactMapper contactMapper,
        PersonMapper personMapper
    ) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
        this.personMapper = personMapper;
    }

    @Override
    public List<ContactDto> findAll(String personId) {
        try {
            return contactRepository.findAllByPerson_PersonId(personId).stream()
                .map(contactMapper::toContactResponse)
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't find contacts for person:" + personId);
        }
    }

    @Override
    @Transactional
    public List<ContactDto> create(List<ContactCreateRequest> contacts, PersonDto personDto) {
        Person person = personMapper.fromDto(personDto);
        contacts.stream()
            .filter(it -> contactRepository.existsByValue(it.getValue()))
            .findAny()
            .ifPresent(a -> {throw new AlreadyExistsException("Contact: " + a.getValue() + " already exists");});
        try {
            List<Contact> created = contactRepository.saveAll(contacts.stream()
                .map(it -> contactMapper.toContact(it, person))
                .collect(Collectors.toList())
            );
            return created.stream()
                .map(contactMapper::toContactResponse)
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't create contacts");
        }
    }

    @Override
    public ContactDto findPhone(String personId) {
        try {
            return contactMapper.toContactResponse(contactRepository.findByPerson_PersonIdAndType(
                personId,
                Contact.ContactType.PHONE
            ));
        } catch (Exception e) {
            throw new DBException("Can't find phone for person: " + personId);
        }
    }
}
