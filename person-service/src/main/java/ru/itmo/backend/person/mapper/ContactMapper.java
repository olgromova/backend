package ru.itmo.backend.person.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.person.dto.ContactCreateRequest;
import ru.itmo.backend.person.dto.ContactDto;
import ru.itmo.backend.person.model.Contact;
import ru.itmo.backend.person.model.Person;

@Mapper(componentModel = "spring")
public interface ContactMapper {
    @Mapping(target = "personId", source = "contact.person.personId")
    ContactDto toContactResponse(Contact contact);

    @Mapping(target = "contactId", expression = "java(java.util.UUID.randomUUID().toString())")
    Contact toContact(ContactCreateRequest contactCreateRequest, Person person);
}
