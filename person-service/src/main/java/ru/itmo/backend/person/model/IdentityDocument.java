package ru.itmo.backend.person.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Getter
@Setter
@Entity(name = "identity_document")
@Table(name = "documents")
@AllArgsConstructor
@NoArgsConstructor
public class IdentityDocument {
    @Id
    private String documentId;

    @Column
    private String number;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @Column
    private boolean main;

    @Column(columnDefinition = "document_type")
    @Enumerated(EnumType.STRING)
    private DocumentType type;

    public enum DocumentType {
        PASSPORT, INTERNATIONAL_PASSPORT, SNILS, INSURANCE_POLICY
    }
}
