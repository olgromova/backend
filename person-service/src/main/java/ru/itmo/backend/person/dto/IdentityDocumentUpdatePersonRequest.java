package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.IdentityDocument;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

@Getter
@Setter
public class IdentityDocumentUpdatePersonRequest extends WithMain {
    @NotBlank
    @Size(min = 1, max = 50)
    private String documentId;

    @NotBlank
    private String number;

    private boolean main;

    private IdentityDocument.DocumentType type;
}
