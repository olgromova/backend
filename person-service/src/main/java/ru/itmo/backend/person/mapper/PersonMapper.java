package ru.itmo.backend.person.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.person.dto.AddressDto;
import ru.itmo.backend.person.dto.ContactDto;
import ru.itmo.backend.person.dto.IdentityDocumentDto;
import ru.itmo.backend.person.dto.PersonCreateRequest;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.dto.PersonDtoSimple;
import ru.itmo.backend.person.dto.PersonUpdateRequest;
import ru.itmo.backend.person.model.Person;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonMapper {
    @Mapping(target = "documents", source = "documents")
    @Mapping(target = "contacts", source = "contacts")
    @Mapping(target = "addresses", source = "addresses")
    PersonDto toPersonResponse(
        Person person,
        List<IdentityDocumentDto> documents,
        List<ContactDto> contacts,
        List<AddressDto> addresses
    );

    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "contacts", ignore = true)
    @Mapping(target = "addresses", ignore = true)
    PersonDto toPersonResponse(
        Person person
    );

    @Mapping(target = "show", ignore = true)
    @Mapping(target = "mainAddress", ignore = true)
    @Mapping(target = "personId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "addresses", ignore = true)
    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "contacts", ignore = true)
    Person toPerson(PersonCreateRequest personCreateRequest);

    @Mapping(target = "show", ignore = true)
    @Mapping(target = "mainAddress", ignore = true)
    Person fromDto(PersonDto personDto);

    @Mapping(target = "personId", source = "personDto.personId")
    @Mapping(target = "name", source = "personDto.name")
    PersonDtoSimple toPersonSimpleResponse(
        PersonDto personDto,
        IdentityDocumentDto mainDocument,
        ContactDto phone,
        AddressDto registrationAddress
    );


    @Mapping(target = "mainAddress", ignore = true)
    @Mapping(target = "addresses", ignore = true)
    Person toPerson(PersonUpdateRequest personUpdateRequest);
}
