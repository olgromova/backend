package ru.itmo.backend.person.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.person.model.Contact;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, String> {
    List<Contact> findAllByPerson_PersonId(String personId);

    Contact findByPerson_PersonIdAndType(String personId, Contact.ContactType type);

    boolean existsByValue(String value);
}
