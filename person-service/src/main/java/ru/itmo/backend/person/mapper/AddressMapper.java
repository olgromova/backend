package ru.itmo.backend.person.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.person.dto.AddressCreateRequest;
import ru.itmo.backend.person.dto.AddressDto;
import ru.itmo.backend.person.dto.AddressUpdatePersonRequest;
import ru.itmo.backend.person.dto.RegionDto;
import ru.itmo.backend.person.model.Address;
import ru.itmo.backend.person.model.Person;
import ru.itmo.backend.person.model.Region;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    @Mapping(target = "region", source = "regionDto")
    @Mapping(target = "name", source = "address.name")
    AddressDto toAddressResponse(Address address, RegionDto regionDto);

    @Mapping(target = "addressId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "region", source= "region")
    @Mapping(target = "name", source = "addressCreateRequest.name")
    Address toAddress(AddressCreateRequest addressCreateRequest, Region region, List<Person> persons);

    @Mapping(target = "persons", ignore = true)
    Address fromDto(AddressDto addressDto);

    @Mapping(target = "addressId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "region", source= "region")
    @Mapping(target = "name", source = "address.name")
    Address toAddress(AddressUpdatePersonRequest address, Region region, List<Person> persons);
}
