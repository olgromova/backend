package ru.itmo.backend.person.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PersonDto {
    private String personId;

    private String name;

    private List<IdentityDocumentDto> documents;

    private List<ContactDto> contacts;

    private List<AddressDto> addresses;

    private LocalDate birthday;
}
