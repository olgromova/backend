package ru.itmo.backend.person.service.impl;

import org.springframework.stereotype.Service;
import ru.itmo.backend.person.exceptions.DBException;
import ru.itmo.backend.person.repository.IdentityDocumentRepository;
import ru.itmo.backend.person.service.IdentityDocumentService;
import ru.itmo.backend.person.dto.IdentityDocumentCreateRequest;
import ru.itmo.backend.person.dto.IdentityDocumentDto;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.mapper.IdentityDocumentMapper;
import ru.itmo.backend.person.mapper.PersonMapper;
import ru.itmo.backend.person.model.IdentityDocument;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IdentityDocumentServiceImpl implements IdentityDocumentService {
    private final IdentityDocumentRepository identityDocumentRepository;
    private final IdentityDocumentMapper identityDocumentMapper;
    private final PersonMapper personMapper;

    public IdentityDocumentServiceImpl(
        IdentityDocumentRepository identityDocumentRepository,
        IdentityDocumentMapper identityDocumentMapper,
        PersonMapper personMapper
    ) {
        this.identityDocumentRepository = identityDocumentRepository;
        this.identityDocumentMapper = identityDocumentMapper;
        this.personMapper = personMapper;
    }

    @Override
    public List<IdentityDocumentDto> create(List<IdentityDocumentCreateRequest> identityDocumentCreateRequest, PersonDto person) {
        try {
            List<IdentityDocument> documents = identityDocumentRepository.saveAll(identityDocumentCreateRequest.stream()
                .map(it -> identityDocumentMapper.toIdentityDocument(it, personMapper.fromDto(person)))
                .collect(Collectors.toList()));
            return documents.stream()
                .map(it -> identityDocumentMapper.toIdentityDocumentResponse(it, person.getPersonId()))
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't create documents");
        }
    }

    @Override
    public IdentityDocumentDto mainDocument(String personId) {
        try {
            return identityDocumentMapper.toIdentityDocumentResponse(
                identityDocumentRepository.findMainDocument(personId),
                personId
            );
        } catch (Exception e) {
            throw new DBException("Can't find main document for person: " + personId);
        }
    }

    @Override
    public List<IdentityDocumentDto> findAll(String personId) {
        try {
            return identityDocumentRepository.findAllByPerson_PersonId(personId)
                .stream()
                .map(it -> identityDocumentMapper.toIdentityDocumentResponse(it, personId))
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can.t find documents for person: " + personId);
        }
    }
}
