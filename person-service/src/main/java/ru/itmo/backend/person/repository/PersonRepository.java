package ru.itmo.backend.person.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.person.model.Address;
import ru.itmo.backend.person.model.Person;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface PersonRepository extends JpaRepository<Person, String> {
    @Modifying
    @Transactional
    @Query("update person p set p.mainAddress = :mainAddress where p.personId = :personId")
    void updateMainAddress(@Param("personId") String personId, @Param("mainAddress") Address mainAddress);

    List<Person> findAllByShowTrue();

    @Query("select case when count(d)>0 then true else false end from identity_document d join d.person p where p.name=:name and d.number=:number and d.main=true")
    boolean existsMainDocument(@Param("name") String name, @Param("number") String number);

    @Query("select p.personId from person p where p.mainAddress.region.name=:region and p.show=true")
    List<String> findAllIdsByRegionAndShowTrue(@Param("region") String region, Pageable pageable);

    @Query("from person p join fetch p.addresses a join fetch a.region join fetch p.mainAddress join fetch p.documents join fetch p.contacts where p.personId in :ids")
    List<Person> findAllPersonsInIds(@Param("ids") Set<String> ids);

    @Query("from person p join fetch p.documents d where d.type='PASSPORT' and d.number= :passport")
    Optional<Person> findByPassport(@Param("passport") String passport);

    @Query("select p.personId from person p where p.show=true")
    List<String> findAllIdsByShowTrue(Pageable pageable);
}
