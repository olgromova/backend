package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.model.IdentityDocument;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Getter
@Setter
public class IdentityDocumentCreateRequest extends WithMain {
    @NotBlank
    private String number;

    @NotNull
    private boolean main;

    @NotNull
    private IdentityDocument.DocumentType type;
}
