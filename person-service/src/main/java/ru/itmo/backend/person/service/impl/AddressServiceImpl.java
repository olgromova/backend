package ru.itmo.backend.person.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.person.exceptions.DBException;
import ru.itmo.backend.person.repository.AddressRepository;
import ru.itmo.backend.person.repository.RegionRepository;
import ru.itmo.backend.person.service.AddressService;
import ru.itmo.backend.person.dto.AddressCreateRequest;
import ru.itmo.backend.person.dto.AddressDto;
import ru.itmo.backend.person.dto.AddressUpdatePersonRequest;
import ru.itmo.backend.person.dto.PersonDto;
import ru.itmo.backend.person.mapper.AddressMapper;
import ru.itmo.backend.person.mapper.PersonMapper;
import ru.itmo.backend.person.mapper.RegionMapper;
import ru.itmo.backend.person.model.Address;
import ru.itmo.backend.person.model.Person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {
    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;
    private final RegionMapper regionMapper;
    private final RegionRepository regionRepository;
    private final PersonMapper personMapper;


    public AddressServiceImpl(
        AddressRepository addressRepository,
        AddressMapper addressMapper,
        RegionMapper regionMapper,
        RegionRepository regionRepository,
        PersonMapper personMapper
    ) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
        this.regionMapper = regionMapper;
        this.regionRepository = regionRepository;
        this.personMapper = personMapper;
    }

    @Override
    public List<AddressDto> findAll(String personId) {
        try {
            List<Address> addresses = addressRepository.findAllByPersonId(personId);
            return addresses.stream()
                .map(it -> addressMapper.toAddressResponse(it, regionMapper.toRegionResponse(it.getRegion())))
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't find all addresses for person: " + personId);
        }
    }

    @Override
    @Transactional
    public List<AddressDto> create(List<AddressCreateRequest> addressCreateRequests, PersonDto personDto) {
        try {
            List<Address> created = new ArrayList<>();
            Person person = personMapper.fromDto(personDto);
            for (AddressCreateRequest addressCreateRequest : addressCreateRequests) {
                var regiono = regionRepository.findByName(addressCreateRequest.getRegion());
                if (regiono.isEmpty()) {
                    regiono = Optional.of(regionRepository.save(regionMapper.toRegion(addressCreateRequest.getRegion())));
                }
                var region = regiono.get();
                //.orElse(regionRepository.save(regionMapper.toRegion(addressCreateRequest.getRegion())));
                Optional<Address> addressOptional = addressRepository.findByName(addressCreateRequest.getName());
                Address address;
                if (addressOptional.isEmpty()) {
                    address = addressMapper.toAddress(
                        addressCreateRequest,
                        region,
                        List.of(person)
                    );
                } else {
                    address = addressOptional.get();
                    Set<Person> persons = new HashSet<>(address.getPersons());
                    persons.add(person);
                    address.setPersons(persons);
                }
                created.add(addressRepository.save(address));
            }
            return created.stream()
                .map(it -> addressMapper.toAddressResponse(it, regionMapper.toRegionResponse(it.getRegion())))
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't create addresses. Something went wrong");
        }
    }

    @Override
    @Transactional
    public List<AddressDto> updatePerson(
        List<AddressUpdatePersonRequest> addressUpdatePersonRequests, PersonDto personDto
    ) {
        try {
            List<Address> created = new ArrayList<>();
            Person person = personMapper.fromDto(personDto);
            for (AddressUpdatePersonRequest addressUpdatePersonRequest : addressUpdatePersonRequests) {
                var regiono = regionRepository.findByName(addressUpdatePersonRequest.getRegion());
                if (regiono.isEmpty()) {
                    regiono = Optional.of(regionRepository.save(regionMapper.toRegion(addressUpdatePersonRequest.getRegion())));
                }
                var region = regiono.get();
                Optional<Address> addressOptional = addressRepository.findByName(addressUpdatePersonRequest.getName());
                if (addressOptional.isEmpty()) {
                    Address address = addressMapper.toAddress(
                        addressUpdatePersonRequest,
                        region,
                        List.of(person)
                    );
                    created.add(addressRepository.save(address));
                } else {
                    created.add(addressOptional.get());
                }
            }
            return created.stream()
                .map(it -> addressMapper.toAddressResponse(it, regionMapper.toRegionResponse(it.getRegion())))
                .collect(Collectors.toList());
        } catch (Exception e) {
            throw new DBException("Can't update address");
        }
    }
}
