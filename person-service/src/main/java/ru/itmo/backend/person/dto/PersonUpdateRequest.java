package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itmo.backend.person.validators.OnlyOneMain;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class PersonUpdateRequest {
    @NotBlank
    @Size(min = 1, max = 50)
    private String personId;

    @NotBlank
    private String name;

    @OnlyOneMain(message = "only one main document must be")
    @Valid
    private List<IdentityDocumentUpdatePersonRequest> documents;

    @OnlyOneMain(message = "only one main document must be")
    @Valid
    private List<AddressUpdatePersonRequest> addresses;

    @Valid
    private List<ContactUpdatePersonRequest> contacts;

    private LocalDate birthday;

    private boolean show = true;
}
