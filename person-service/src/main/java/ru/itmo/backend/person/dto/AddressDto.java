package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto {
    private String addressId;

    private String name;

    private RegionDto region;
}
