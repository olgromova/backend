package ru.itmo.backend.person.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionDto {
    private String regionId;

    private String name;
}
