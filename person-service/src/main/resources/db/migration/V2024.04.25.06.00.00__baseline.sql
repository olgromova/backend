create table if not exists regions
(
    region_id varchar(50) not null primary key,
    name      varchar(20) not null unique
);

create table if not exists address
(
    address_id varchar(50) not null primary key,
    name       varchar(50) not null unique,
    region_id  varchar(50) not null references regions (region_id)
);

create table if not exists persons
(
    person_id       varchar(50) not null primary key,
    name            text        not null,
    main_address_id varchar(50) references address (address_id),
    birthday        date        not null,
    show            bool        not null default true
);

create table if not exists persons_address
(
    person_id  varchar(50) not null references persons (person_id) on delete cascade,
    address_id varchar(50) not null references address (address_id) on delete cascade,
    primary key (person_id, address_id)
);

create type document_type as enum ('PASSPORT', 'INTERNATIONAL_PASSPORT', 'SNILS', 'INSURANCE_POLICY');

create table if not exists documents
(
    document_id varchar(50)   not null primary key,
    type        document_type not null,
    number      text          not null,
    person_id   varchar(50)   not null references persons (person_id) on delete cascade,
    main        bool          not null default false
);

create type contact_type as enum ('PHONE', 'EMAIL', 'WORK_PHONE');

create table if not exists contacts
(
    contact_id varchar(50)  not null primary key,
    type       contact_type not null,
    value      varchar(20)  not null unique,
    person_id  varchar(50)  not null references persons (person_id) on delete cascade
);



