package ru.itmo.backend.qr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.qr.model.Qr;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface QrRepository extends JpaRepository<Qr, String> {
    @Query("select q.code from qr q where q.passport = :passport order by q.vaccinationDate desc")
    List<String> getCode(@Param("passport") String passport);

    Optional<Qr> findByCode(String code);

    @Modifying
    @Transactional
    @Query(value = "insert into qr (qr_id, passport, code, vaccination_id, vaccination_date) values (?1, ?2, ?3, ?4, ?5) on conflict do nothing", nativeQuery = true)
    void saveWithSkipOnConflict(
        String qr_id,
        String passport,
        String code,
        String vaccinationId,
        LocalDate vaccinationDate
    );
}
