package ru.itmo.backend.qr.exceptions;

public class DBException extends RuntimeException {

    public DBException(String message) {
        super(message);
    }
}
