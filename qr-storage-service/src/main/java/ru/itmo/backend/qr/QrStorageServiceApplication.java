package ru.itmo.backend.qr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrStorageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(QrStorageServiceApplication.class, args);
    }

}

