package ru.itmo.backend.qr.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.qr.dto.VaccinationInfo;
import ru.itmo.backend.qr.service.QrService;
import ru.itmo.backend.qr.exceptions.DBException;
import ru.itmo.backend.qr.exceptions.NotFoundException;
import ru.itmo.backend.qr.mapper.QrMapper;
import ru.itmo.backend.qr.model.Qr;
import ru.itmo.backend.qr.repository.QrRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class QrServiceImpl implements QrService {
    private final QrRepository qrRepository;
    private final QrMapper qrMapper;

    public QrServiceImpl(QrRepository qrRepository, QrMapper qrMapper) {
        this.qrRepository = qrRepository;
        this.qrMapper = qrMapper;
    }

    @Override
    public String getCode(String passport) {
        List<String> codes = qrRepository.getCode(passport);
        if (codes.isEmpty()) {
            throw new NotFoundException("code for passport not found");
        }
        return codes.get(0);
    }

    @Override
    @Transactional
    public boolean checkCode(String code) {
        try {
            Optional<Qr> qr = qrRepository.findByCode(code);
            if (qr.isEmpty()) {
                return false;
            }
            String lastCode = getCode(qr.get().getPassport());
            return Objects.equals(lastCode, qr.get().getCode());
        } catch (NotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new DBException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public void saveCode(VaccinationInfo vaccinationInfo) {
        Qr qr = qrMapper.fromProto(vaccinationInfo);
        saveWithSkipOnConflict(qr);
    }

    private void saveWithSkipOnConflict(Qr qr) {
        qrRepository.saveWithSkipOnConflict(
            qr.getQrId(),
            qr.getPassport(),
            qr.getCode(),
            qr.getVaccinationId(),
            qr.getVaccinationDate()
        );
    }
}
