package ru.itmo.backend.qr.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.DefaultErrorHandler;
import ru.itmo.backend.qr.deserializer.VaccinationInfoDeserializer;
import ru.itmo.backend.qr.dto.VaccinationInfo;

import java.util.HashMap;

@EnableKafka
@Configuration
@Slf4j
public class KafkaConsumerConfiguration {

    @Bean
    public ConsumerFactory<String, VaccinationInfo> consumerFactory(
        @Value("${kafka.server.url}") String kafkaUrl,
        @Value("${kafka.group.id}") String groupId
    ) {
        var config = new HashMap<String, Object>();
        log.info("kafkaUrl: {}", kafkaUrl);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, VaccinationInfoDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(config);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, VaccinationInfo> kafkaListenerContainerFactory(
        @Value("${kafka.server.url}") String kafkaUrl,
        @Value("${kafka.group.id}") String groupId
    ) {
        ConcurrentKafkaListenerContainerFactory<String, VaccinationInfo> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory(kafkaUrl, groupId));
        factory.setConcurrency(1);
        factory.setCommonErrorHandler(new DefaultErrorHandler());
        return factory;
    }
}