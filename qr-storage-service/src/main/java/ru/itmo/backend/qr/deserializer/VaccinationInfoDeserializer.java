package ru.itmo.backend.qr.deserializer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;
import ru.itmo.backend.qr.dto.VaccinationInfo;

import java.io.IOException;

@Slf4j
public class VaccinationInfoDeserializer implements Deserializer<VaccinationInfo> {
    private final ObjectMapper objectMapper;

    public VaccinationInfoDeserializer() {
        this.objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JSR310Module());
        objectMapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
    }

    @Override
    public VaccinationInfo deserialize(final String topic, byte[] data) {
        try {
            return objectMapper.readValue(data, VaccinationInfo.class);
        }  catch (IOException e) {
            log.error("Can't deserialize VaccinationInfo", e);
            return null;
        }
    }
}