package ru.itmo.backend.qr.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itmo.backend.qr.exceptions.DBException;
import ru.itmo.backend.qr.exceptions.NotFoundException;

import java.time.LocalDateTime;
import java.util.Map;

@ControllerAdvice
@Slf4j
public class ExceptionsControllerAdvice {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleException(NotFoundException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", message));
    }

    @ExceptionHandler(DBException.class)
    public ResponseEntity<?> handleException(DBException e) {
        log.error(e.getMessage());
        return ResponseEntity.internalServerError().body(Map.of("error", e.getMessage()));
    }
}