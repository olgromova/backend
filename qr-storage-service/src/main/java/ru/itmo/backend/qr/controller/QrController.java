package ru.itmo.backend.qr.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itmo.backend.qr.service.QrService;

@RestController
@RequestMapping("/qr")
public class QrController {
    private final QrService qrService;

    public QrController(QrService qrService) {this.qrService = qrService;}

    @GetMapping("/{passport}")
    public String getCode(@PathVariable String passport) {
        return qrService.getCode(passport);
    }

    @GetMapping("/check")
    public boolean checkCode(@RequestParam("code") String code) {
        return qrService.checkCode(code);
    }
}
