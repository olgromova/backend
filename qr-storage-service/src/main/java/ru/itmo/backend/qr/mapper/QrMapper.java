package ru.itmo.backend.qr.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.qr.dto.VaccinationInfo;
import ru.itmo.backend.qr.model.Qr;

@Mapper(componentModel = "spring")
public interface QrMapper {

    @Mapping(target = "qrId", expression = "java(java.util.UUID.randomUUID().toString())")
    @Mapping(target = "code", expression = "java(org.apache.commons.codec.digest.DigestUtils.md5Hex(vaccinationInfo.getVaccinationId() + vaccinationInfo.getPassport()).toUpperCase())")
    @Mapping(target = "vaccinationId", source = "vaccinationInfo.vaccinationId")
    @Mapping(target = "vaccinationDate", source = "vaccinationInfo.vaccinationDate")
    Qr fromProto(VaccinationInfo vaccinationInfo);
}
