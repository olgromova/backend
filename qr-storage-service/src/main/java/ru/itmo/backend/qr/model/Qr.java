package ru.itmo.backend.qr.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.LocalDate;

@Entity(name = "qr")
@Table(name = "qr")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Qr {
    @Id
    private String qrId;

    @Column
    private String passport;

    @Column
    private String code;

    @Column
    private String vaccinationId;

    @Column
    private LocalDate vaccinationDate;
}
