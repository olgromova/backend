package ru.itmo.backend.qr.service;

import ru.itmo.backend.qr.dto.VaccinationInfo;

public interface QrService {
    String getCode(String passport);

    boolean checkCode(String code);

    void saveCode(VaccinationInfo vaccinationInfo);
}
