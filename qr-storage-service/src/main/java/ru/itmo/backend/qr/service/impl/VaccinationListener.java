package ru.itmo.backend.qr.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.itmo.backend.qr.dto.VaccinationInfo;
import ru.itmo.backend.qr.service.QrService;

@Slf4j
@Service
public class VaccinationListener {
    private final QrService qrService;

    public VaccinationListener(
        QrService qrService
    ) {
        this.qrService = qrService;
    }


    @KafkaListener(topics = "vaccination.info")
    public void processVaccination(VaccinationInfo vaccinationInfo) {
        qrService.saveCode(vaccinationInfo);
    }
}
