create table if not exists qr
(
    qr_id            varchar(50) not null primary key,
    passport         varchar(50) not null,
    code             varchar(40) not null unique,
    vaccination_id   varchar(50) not null unique,
    vaccination_date date        not null
);

create index if not exists idx_qr_passport on qr (passport);