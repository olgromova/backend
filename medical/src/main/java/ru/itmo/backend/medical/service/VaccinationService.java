package ru.itmo.backend.medical.service;


import jakarta.validation.constraints.NotNull;
import lombok.NonNull;
import ru.itmo.backend.medical.dto.VaccinationDto;
import ru.itmo.backend.medical.dto.VaccinationLoad;

import java.util.List;

public interface VaccinationService {
    VaccinationDto getVaccinationFullInfo(@NonNull String document);

    void saveVaccinations(@NotNull List<VaccinationLoad> vaccinations);
}
