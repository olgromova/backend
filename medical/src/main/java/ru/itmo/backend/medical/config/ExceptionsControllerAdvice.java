package ru.itmo.backend.medical.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itmo.backend.medical.exceptions.BadRequestException;

import java.time.LocalDateTime;
import java.util.Map;

@ControllerAdvice
public class ExceptionsControllerAdvice {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleException(BadRequestException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }
}