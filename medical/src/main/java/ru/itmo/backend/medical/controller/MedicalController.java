package ru.itmo.backend.medical.controller;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.itmo.backend.medical.exceptions.BadRequestException;
import ru.itmo.backend.medical.dto.VaccinationDto;
import ru.itmo.backend.medical.dto.VaccinationLoad;
import ru.itmo.backend.medical.service.VaccinationService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

@RestController
@RequestMapping("/medical")
@Slf4j
public class MedicalController {
    private final VaccinationService vaccinationService;

    public MedicalController(
        VaccinationService vaccinationService
    ) {
        this.vaccinationService = vaccinationService;
    }


    @GetMapping("/vaccination")
    public ResponseEntity<VaccinationDto> getVaccinationInfo(@RequestParam("document") String document) {
        return ResponseEntity.ok(vaccinationService.getVaccinationFullInfo(document));
    }

    @PostMapping(value = "/process-file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void processFile(@RequestParam("file") MultipartFile file) {
        log.info("load data from file: {}, size: {}", file.getName(), file.getSize());
        if (file.isEmpty()) {
            log.info("file {} is empty", file.getName());
            throw new BadRequestException("The file must not be empty.");
        }
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            CsvToBean<VaccinationLoad> csvToBean = new CsvToBeanBuilder<VaccinationLoad>(reader)
                .withType(VaccinationLoad.class)
                .withSeparator(',')
                .withIgnoreLeadingWhiteSpace(true)
                .build();
            vaccinationService.saveVaccinations(csvToBean.parse());
        } catch (IllegalStateException e) {
            throw new BadRequestException("Can't parse csv file");
        } catch (IOException e) {
            throw new BadRequestException("Can't load file");
        }
    }
}
