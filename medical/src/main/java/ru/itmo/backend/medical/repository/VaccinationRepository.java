package ru.itmo.backend.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.medical.domain.Vaccination;

import java.util.List;

@Repository
public interface VaccinationRepository extends JpaRepository<Vaccination, String> {
    @Query("select distinct v from vaccination v join fetch v.vaccinationPoint"
        + " join fetch v.vaccine where v.document=:document order by v.date desc")
    List<Vaccination> findByDocument(@Param("document") String document);
}
