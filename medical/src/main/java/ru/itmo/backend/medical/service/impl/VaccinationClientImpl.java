package ru.itmo.backend.medical.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.medical.domain.VaccinationStatus;
import ru.itmo.backend.medical.repository.VaccinationRepository;
import ru.itmo.backend.medical.repository.VaccinationStatusRepository;
import ru.itmo.backend.medical.domain.Vaccination;
import ru.itmo.backend.medical.service.VaccinationClient;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class VaccinationClientImpl implements VaccinationClient {
    private final VaccinationRepository vaccinationRepository;
    private final VaccinationStatusRepository vaccinationStatusRepository;

    public VaccinationClientImpl(
        VaccinationRepository vaccinationRepository,
        VaccinationStatusRepository vaccinationStatusRepository
    ) {
        this.vaccinationRepository = vaccinationRepository;
        this.vaccinationStatusRepository = vaccinationStatusRepository;
    }

    @Transactional
    public void saveAll(List<Vaccination> vaccinations) {
        log.info("save {} vaccinations", vaccinations.size());
        var vaccinationsSaved = vaccinationRepository.saveAll(vaccinations);
        vaccinationStatusRepository.saveAll(vaccinationsSaved.stream()
            .map(VaccinationStatus::new)
            .collect(Collectors.toList())
        );
    }
}
