package ru.itmo.backend.medical.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.util.UUID;

@Entity(name = "status")
@Table(name = "vaccination_status")
@Getter
@Setter
@NoArgsConstructor
public class VaccinationStatus {
    public VaccinationStatus(Vaccination vaccination) {
        this.id = UUID.randomUUID().toString();
        this.vaccination = vaccination;
        this.sent = false;
    }

    @Id
    private String id;

    @OneToOne
    @JoinColumn(name = "vaccination_id")
    private Vaccination vaccination;

    @Column
    private boolean sent;
}
