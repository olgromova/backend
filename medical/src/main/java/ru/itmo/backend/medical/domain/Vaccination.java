package ru.itmo.backend.medical.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.time.LocalDate;

@Table(name = "vaccination")
@Entity(name = "vaccination")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Vaccination {
    @Id
    private String id;

    @Column
    private LocalDate date;

    @Column(name = "patient_name")
    private String patientName;

    @Column
    private String document;

    @ManyToOne
    @JoinColumn(name = "vaccination_point_id")
    private VaccinationPoint vaccinationPoint;

    @ManyToOne
    @JoinColumn(name = "vaccine")
    private Vaccine vaccine;
}
