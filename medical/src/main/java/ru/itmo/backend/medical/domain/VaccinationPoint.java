package ru.itmo.backend.medical.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "vaccination_point")
@Entity(name = "vaccination_point")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VaccinationPoint {
    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String city;

    @Column
    private String address;
}
