package ru.itmo.backend.medical.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import ru.itmo.backend.medical.dto.VaccinationInfo;
import ru.itmo.backend.medical.serializers.VaccinationInfoSerializer;

import java.util.HashMap;

@Configuration
@Slf4j
public class KafkaProducerConfiguration {

    @Bean
    public ProducerFactory<String, VaccinationInfo> producerFactory(
        @Value("${kafka.server.url}") String kafkaUrl
    ) {
        var configProps = new HashMap<String, Object>();
        log.info("kafka url: {}", kafkaUrl);
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaUrl);
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, VaccinationInfoSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    public KafkaTemplate<String, VaccinationInfo> kafkaTemplate(
        @Value("${kafka.server.url}") String kafkaUrl
    ) {
        return new KafkaTemplate<>(producerFactory(kafkaUrl));
    }
}
