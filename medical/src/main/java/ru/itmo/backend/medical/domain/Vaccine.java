package ru.itmo.backend.medical.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "vaccine")
@Entity(name = "vaccine")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Vaccine {
    @Id
    private String name;
}
