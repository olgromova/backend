package ru.itmo.backend.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.medical.domain.Vaccine;

import java.util.Optional;

@Repository
public interface VaccineRepository extends JpaRepository<Vaccine, String> {
    Optional<Vaccine> findByName(String name);
}
