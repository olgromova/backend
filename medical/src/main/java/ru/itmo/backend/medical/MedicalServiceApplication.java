package ru.itmo.backend.medical;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MedicalServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MedicalServiceApplication.class, args);
    }
}