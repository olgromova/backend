package ru.itmo.backend.medical.client;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "person", fallback = PersonClientFallback.class, url = "${person.service.url}")
public interface PersonClient {
    @GetMapping("/person/verify")
    @CircuitBreaker(name = "personClientCircuitBreaker")
    boolean verify(@RequestParam("name") String name, @RequestParam("passport") String passport);
}

@Slf4j
@Component
class PersonClientFallback implements PersonClient {
    @Override
    public boolean verify(String name, String passport) {
        log.error("Can't connect to person service. Fallback...");
        return false;
    }
}
