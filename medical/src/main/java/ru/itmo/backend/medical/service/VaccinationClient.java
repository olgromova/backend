package ru.itmo.backend.medical.service;

import ru.itmo.backend.medical.domain.Vaccination;

import java.util.List;

public interface VaccinationClient {
    void saveAll(List<Vaccination> vaccinations);
}
