package ru.itmo.backend.medical.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VaccinationDto {
    private String patientName;

    private String document;

    private LocalDate date;

    private String vaccineName;

    private String vaccinationPointId;

    private String vaccinationPointName;

    private String vaccinationPointAddress;
}
