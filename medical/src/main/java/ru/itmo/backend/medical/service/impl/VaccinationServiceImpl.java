package ru.itmo.backend.medical.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itmo.backend.medical.client.PersonClient;
import ru.itmo.backend.medical.domain.Vaccination;
import ru.itmo.backend.medical.domain.VaccinationPoint;
import ru.itmo.backend.medical.domain.Vaccine;
import ru.itmo.backend.medical.dto.VaccinationDto;
import ru.itmo.backend.medical.dto.VaccinationLoad;
import ru.itmo.backend.medical.exceptions.BadRequestException;
import ru.itmo.backend.medical.mapper.VaccinationMapper;
import ru.itmo.backend.medical.repository.VaccinationPointRepository;
import ru.itmo.backend.medical.repository.VaccinationRepository;
import ru.itmo.backend.medical.repository.VaccineRepository;
import ru.itmo.backend.medical.service.VaccinationClient;
import ru.itmo.backend.medical.service.VaccinationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class VaccinationServiceImpl implements VaccinationService {
    private final VaccinationRepository vaccinationRepository;
    private final VaccinationMapper vaccinationMapper;
    private final PersonClient personClient;
    private final VaccinationPointRepository vaccinationPointRepository;
    private final VaccineRepository vaccineRepository;
    private final int batchSize;
    private final VaccinationClient vaccinationClient;

    public VaccinationServiceImpl(
        VaccinationRepository vaccinationRepository,
        VaccinationMapper vaccinationMapper,
        PersonClient personClient,
        VaccinationPointRepository vaccinationPointRepository,
        VaccineRepository vaccineRepository,
        @Value("${spring.jpa.properties.hibernate.jdbc.batch_size}") int batchSize,
        VaccinationClient vaccinationClient
    ) {
        this.vaccinationRepository = vaccinationRepository;
        this.vaccinationMapper = vaccinationMapper;
        this.personClient = personClient;
        this.vaccinationPointRepository = vaccinationPointRepository;
        this.vaccineRepository = vaccineRepository;
        this.batchSize = batchSize;
        this.vaccinationClient = vaccinationClient;
    }

    @Override
    public VaccinationDto getVaccinationFullInfo(@NonNull String document) {
        log.info("Find vaccination by document {}", document);
        List<Vaccination> vaccination = vaccinationRepository.findByDocument(document);
        if (vaccination.isEmpty()) {
            throw new BadRequestException("Vaccination by document " + document + " not found");
        }
        return vaccinationMapper.toDto(vaccination.get(0));
    }

    @Override
    public void saveVaccinations(List<VaccinationLoad> vaccinations) {
        List<Vaccination> vaccinationToSave = new ArrayList<>();
        for (VaccinationLoad vaccinationLoad : vaccinations) {
            log.info("Load vaccination to person {}", vaccinationLoad.getPassport());
            if (!personClient.verify(vaccinationLoad.getFullName(), vaccinationLoad.getPassport())) {
                log.info("Failed to verify person {}", vaccinationLoad.getPassport());
                continue;
            }
            Optional<Vaccine> vaccine = vaccineRepository.findByName(vaccinationLoad.getVaccineName());
            if (vaccine.isEmpty()) {
                log.info("Vaccine {} not found", vaccinationLoad.getVaccineName());
                continue;
            }
            Optional<VaccinationPoint> vaccinationPoint = vaccinationPointRepository.findById(
                vaccinationLoad.getVaccinationPointId()
            );
            if (vaccinationPoint.isEmpty()
                || !Objects.equals(vaccinationPoint.get().getAddress(), vaccinationLoad.getVaccinationPointAddress())
                || !Objects.equals(vaccinationPoint.get().getName(), vaccinationLoad.getVaccinationPointName())
            ) {
                log.info("Vaccination point {} not found", vaccinationLoad.getVaccinationPointId());
                continue;
            }
            Vaccination vaccination = vaccinationMapper.fromLoadAndData(
                vaccinationLoad,
                vaccinationPoint.get(),
                vaccine.get()
            );
            vaccinationToSave.add(vaccination);
            if (vaccinationToSave.size() == batchSize) {
                var vac = new ArrayList<>(vaccinationToSave);
                CompletableFuture.runAsync(() -> vaccinationClient.saveAll(vac));
                vaccinationToSave.clear();
            }
        }
        if (!vaccinationToSave.isEmpty()) {
            vaccinationClient.saveAll(vaccinationToSave);
        }
    }
}
