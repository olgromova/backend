package ru.itmo.backend.medical.mapper;


import ru.itmo.backend.medical.domain.Vaccination;
import ru.itmo.backend.medical.dto.VaccinationInfo;

import java.time.LocalTime;
import java.time.ZoneOffset;

public class VaccinationInfoMapper {
    public static VaccinationInfo toProto(Vaccination vaccination) {
        return VaccinationInfo.builder()
            .vaccinationId(vaccination.getId())
            .passport(vaccination.getDocument())
            .vaccinationDate(vaccination.getDate())
            .build();
    }
}
