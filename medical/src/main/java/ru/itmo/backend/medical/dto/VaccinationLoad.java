package ru.itmo.backend.medical.dto;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VaccinationLoad {
    @CsvBindByName(column = "full_name")
    //@CsvBindByPosition(position = 0)
    private String fullName;

    @CsvBindByName(column = "passport")
    //@CsvBindByPosition(position = 1)
    private String passport;

    @CsvCustomBindByName(column = "date", converter = LocalDateConverter.class)
    //@CsvCustomBindByPosition(position = 2, converter = LocalDateConverter.class)
    private LocalDate vaccinationDate;

    @CsvBindByName(column = "vaccine_name")
    //@CsvBindByPosition(position = 3)
    private String vaccineName;

    @CsvBindByName(column = "vaccination_point_id")
    //@CsvBindByPosition(position = 4)
    private String vaccinationPointId;

    @CsvBindByName(column = "vaccination_point_name")
    //@CsvBindByPosition(position = 5)
    private String vaccinationPointName;

    @CsvBindByName(column = "vaccination_point_address")
    //@CsvBindByPosition(position = 6)
    private String vaccinationPointAddress;

    public static class LocalDateConverter extends AbstractBeanField {
        @Override
        protected Object convert(String s) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withLocale(Locale.US);
            LocalDate parse = LocalDate.parse(s, formatter);
            return parse;
        }
    }
}
