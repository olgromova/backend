package ru.itmo.backend.medical.serializers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import org.apache.kafka.common.serialization.Serializer;
import ru.itmo.backend.medical.dto.VaccinationInfo;

public class VaccinationInfoSerializer implements Serializer<VaccinationInfo> {
    private final ObjectMapper objectMapper;

    public VaccinationInfoSerializer() {
        this.objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JSR310Module());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @Override
    public byte[] serialize(final String topic, final VaccinationInfo data) {
        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}