package ru.itmo.backend.medical.dto;


import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VaccinationInfo {
    private String vaccinationId;

    private String passport;

    private LocalDate vaccinationDate;
}
