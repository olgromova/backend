package ru.itmo.backend.medical.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.itmo.backend.medical.domain.VaccinationStatus;
import ru.itmo.backend.medical.dto.VaccinationInfo;
import ru.itmo.backend.medical.mapper.VaccinationInfoMapper;
import ru.itmo.backend.medical.repository.VaccinationStatusRepository;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class VaccinationSendService {
    private final String kafkaTopic;
    private final KafkaTemplate<String, VaccinationInfo> kafkaProducer;
    private final VaccinationStatusRepository vaccinationStatusRepository;

    public VaccinationSendService(
            @Value("${kafka.topic}") String kafkaTopic,
            KafkaTemplate<String, VaccinationInfo> kafkaProducer,
            VaccinationStatusRepository vaccinationStatusRepository
    ) {
        this.kafkaTopic = kafkaTopic;
        this.kafkaProducer = kafkaProducer;
        this.vaccinationStatusRepository = vaccinationStatusRepository;
    }

    @Scheduled(fixedDelayString = "${scheduler.interval}")
    @SchedulerLock(name = "sendEventLock")
    public void sendEvent() {
        log.info("Started check for new vaccination events");
        List<VaccinationStatus> vaccinationStatuses = vaccinationStatusRepository.getAllNotSent();
        for (VaccinationStatus vaccinationStatus : vaccinationStatuses) {
            final String id = vaccinationStatus.getId();
            log.info("Find vaccination: {}", id);
            final VaccinationInfo vaccinationInfo = VaccinationInfoMapper.toProto(vaccinationStatus.getVaccination());
            ProducerRecord<String, VaccinationInfo> record = new ProducerRecord<>(kafkaTopic, vaccinationInfo.getVaccinationId(), vaccinationInfo);
            CompletableFuture<SendResult<String, VaccinationInfo>> futureResult = kafkaProducer.send(record)
                    .whenComplete((result, ex) -> {
                        if (ex != null) {
                            log.error("Failed to send message.", ex);
                        } else {
                            vaccinationStatusRepository.setSent(id);
                            log.info("Sent {}, vaccination: {}", id, vaccinationInfo.getVaccinationId());
                        }
                    });

            try {
                futureResult.get();
            } catch (InterruptedException e) {
                log.warn("Interrupt send event\n{}", e.getMessage());
            } catch (ExecutionException e) {
                log.error("Failed to send record\n{}", e.getMessage());
            }
        }
        log.info("Finish check for new vaccination events");
    }
}
