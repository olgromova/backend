package ru.itmo.backend.medical.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.itmo.backend.medical.domain.VaccinationPoint;
import ru.itmo.backend.medical.domain.Vaccination;
import ru.itmo.backend.medical.domain.Vaccine;
import ru.itmo.backend.medical.dto.VaccinationDto;
import ru.itmo.backend.medical.dto.VaccinationLoad;

@Mapper(componentModel = "spring")
public interface VaccinationMapper {
    @Mapping(target = "vaccineName", source = "vaccination.vaccine.name")
    @Mapping(target = "vaccinationPointName", source = "vaccination.vaccinationPoint.name")
    @Mapping(target = "vaccinationPointId", source = "vaccination.vaccinationPoint.id")
    @Mapping(target = "vaccinationPointAddress", source = "vaccination.vaccinationPoint.address")
    VaccinationDto toDto(Vaccination vaccination);

    @Mapping(target = "patientName", source = "vaccinationLoad.fullName")
    @Mapping(target = "document", source = "vaccinationLoad.passport")
    @Mapping(target = "date", source = "vaccinationLoad.vaccinationDate")
    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID().toString())")
    Vaccination fromLoadAndData(VaccinationLoad vaccinationLoad, VaccinationPoint vaccinationPoint, Vaccine vaccine);
}
