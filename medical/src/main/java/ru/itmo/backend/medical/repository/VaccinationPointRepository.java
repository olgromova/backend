package ru.itmo.backend.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itmo.backend.medical.domain.VaccinationPoint;

@Repository
public interface VaccinationPointRepository extends JpaRepository<VaccinationPoint, String> {
}
