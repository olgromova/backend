package ru.itmo.backend.medical.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.itmo.backend.medical.domain.VaccinationStatus;

import java.util.List;

@Repository
public interface VaccinationStatusRepository extends JpaRepository<VaccinationStatus, String> {
    @Query("select s from status s join fetch s.vaccination v join fetch v.vaccine join fetch v.vaccinationPoint where s.sent=false")
    List<VaccinationStatus> getAllNotSent();

    @Modifying
    @Transactional
    @Query("update status s set s.sent=true where s.id=:id")
    void setSent(@Param("id") String id);
}
