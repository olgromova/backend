create table if not exists vaccine
(
    name varchar(20) not null primary key
);

create table if not exists vaccination_point
(
    id      varchar(50) not null primary key,
    name    varchar(50) not null,
    city    varchar(20) not null,
    address varchar(50) not null
);

create table if not exists vaccination
(
    id                   varchar(50)  not null primary key,
    date                 date         not null,
    patient_name         varchar(100) not null,
    document             varchar(50)  not null,
    vaccination_point_id varchar(50)  not null references vaccination_point (id),
    vaccine              varchar(20)  not null references vaccine (name)
);