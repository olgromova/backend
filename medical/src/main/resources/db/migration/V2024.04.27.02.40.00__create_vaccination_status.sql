create table if not exists vaccination_status
(
    id             varchar(50) not null primary key,
    vaccination_id varchar(50) not null references vaccination (id),
    sent           bool        not null default false
);

create index if not exists idx_vaccination_status_new on vaccination_status (sent);